﻿
namespace InquiryApp.Common.Enums
{
    public enum TransactionStatus
    {
        Success = 1,
        Failed,
        Canceled
    }
}
