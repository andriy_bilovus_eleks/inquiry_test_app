﻿using System;
using InquiryApp.Common.Enums;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace InquiryApp.DataServices.Entities
{
    public class TransactionEntity
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        [MaxLength(3)]
        public string CurrencyCode { get; set; }

        public TransactionStatus Status { get; set; }
    }
}
