﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InquiryApp.DataServices.Entities
{
    public class CustomerEntity
    {
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; }

        [EmailAddress]
        [MaxLength(25)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        public virtual ICollection<TransactionEntity> Transactions { get; set; }
    }
}
