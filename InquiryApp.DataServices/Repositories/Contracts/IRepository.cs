﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.DataServices.Repositories.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity[]> GetAllAsync();

        void Add(TEntity entity);

        void Delete(TEntity entity);

        void MarkUpdate(TEntity entity);

        Task CommitAsync();
    }
}
