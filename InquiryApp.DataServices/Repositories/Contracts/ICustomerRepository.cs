﻿using InquiryApp.DataServices.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.DataServices.Repositories.Contracts
{
    public interface ICustomerRepository : IRepository<CustomerEntity>
    {
        Task<CustomerEntity> GetByCondition(Expression<Func<CustomerEntity, bool>> condition);
    }
}
