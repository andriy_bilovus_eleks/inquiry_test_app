﻿using InquiryApp.DataServices.Context;
using InquiryApp.DataServices.Entities;
using InquiryApp.DataServices.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.DataServices.Repositories
{
    public class CustomerRepository : BaseRepository<CustomerEntity>, ICustomerRepository
    {
        public CustomerRepository(InquiryDbContext context) : base(context)
        {
        }

        public async Task<CustomerEntity> GetByCondition(Expression<Func<CustomerEntity, bool>> condition)
        {
            return await _context.Customers.Include(c => c.Transactions).Where(condition).FirstOrDefaultAsync();
        }
    }
}
