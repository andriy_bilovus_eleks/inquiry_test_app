﻿using InquiryApp.DataServices.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace InquiryApp.DataServices.Context
{
    public class InquiryDbContext : DbContext
    {
        public InquiryDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<TransactionEntity> Transactions { get; set; }

        public DbSet<CustomerEntity> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TransactionEntity>().Property(t => t.Amount).HasPrecision(15, 2);

            builder.Entity<CustomerEntity>().HasIndex(c => c.Email).IsUnique();
        }
    }
}
