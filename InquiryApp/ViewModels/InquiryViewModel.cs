﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InquiryApp.Web.ViewModels
{
    public class CustomerInquiryViewModel
    {
        public int? Id { get; set; }

        public string Email { get; set; }
    }
}
