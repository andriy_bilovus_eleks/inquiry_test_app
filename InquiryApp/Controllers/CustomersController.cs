﻿using InquiryApp.AppServices.DomainModels;
using InquiryApp.AppServices.Services.Contracts;
using InquiryApp.Common.Validators;
using InquiryApp.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace InquiryApp.Web.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IInquiryService _inquiryService;
        private readonly ICustomerService _customerService;

        public CustomersController(IInquiryService inquiryService, ICustomerService customerService)
        {
            _inquiryService = inquiryService;
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            return Ok(await _customerService.GetCustomers());
        }

        [HttpGet]
        [Route("inquiry")]
        public async Task<IActionResult> GetCustomerByInquery([FromQuery]CustomerInquiryViewModel customerInquiry)
        {
            if (customerInquiry == null)
            {
                return BadRequest("No inquiry criteria");
            }

            if (!customerInquiry.Id.HasValue && String.IsNullOrEmpty(customerInquiry.Email))
            {
                return BadRequest("No inquiry criteria");
            }

            if (customerInquiry.Id.HasValue && customerInquiry.Id <= 0)
            {
                return BadRequest("Invalid Customer ID");
            }

            if (!String.IsNullOrEmpty(customerInquiry.Email) && !RegexpUtilities.ValidateEmail(customerInquiry.Email))
            {
                return BadRequest("Invalid Email");
            }

            try
            {
                Customer customer = await _inquiryService.GetCustomers(customerInquiry.Id, customerInquiry.Email);

                if (customer == null)
                {
                    return NotFound();
                }
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody]Customer customer)
        {
            var createdCustomer = await _customerService.AddCustomer(customer);

            return Ok(createdCustomer);
        }

        [HttpPut]
        [Route("")]
        public async Task<IActionResult> Update([FromBody]Customer customer)
        {
            var updatedCustomer = await _customerService.UpdateCustomer(customer);

            return Ok(updatedCustomer);
        }
    }
}
