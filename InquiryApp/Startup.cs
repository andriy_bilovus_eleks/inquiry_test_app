﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InquiryApp.DataServices.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using InquiryApp.AppServices.Mapper.Contracts;
using InquiryApp.AppServices.Mapper;
using InquiryApp.AppServices.Services;
using InquiryApp.AppServices.Services.Contracts;
using InquiryApp.DataServices.Repositories;
using InquiryApp.DataServices.Repositories.Contracts;
using Swashbuckle.AspNetCore.Swagger;

namespace InquiryApp.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<InquiryDbContext>((provider, builder) =>
            {
                string connectionString = Configuration.GetValue<string>("ConnectionString");
                builder.UseSqlServer(connectionString);
            });

            services.AddSingleton<IObjectMapper>(InquiryMapperBuilder.Build(new InquiryProfile()));

            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IInquiryService, InquiryService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Inquiry API", Version = "v1" });
            });
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IObjectMapper>(InquiryMapperBuilder.Build(new InquiryProfile()));

            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IInquiryService, InquiryService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Inquiry API V1");
                c.RoutePrefix = "api/swagger";
            });

            app.UseMvc();
        }
    }
}
