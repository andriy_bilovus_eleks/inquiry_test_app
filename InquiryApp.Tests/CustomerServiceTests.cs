﻿using FakeItEasy;
using InquiryApp.AppServices.Mapper;
using InquiryApp.AppServices.Services;
using InquiryApp.DataServices.Entities;
using InquiryApp.DataServices.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Xunit;

namespace InquiryApp.Tests
{
    public class CustomerServiceTests
    {

        [Fact]
        public void GetCustomers_FetchesAll()
        {
            //Arrange
            var mockEntities = new CustomerEntity[]
            {
                new CustomerEntity { Id = 1, Email = "test1@gmail.com", MobileNumber = "0673300666", Name = "Test user 1" },
                new CustomerEntity { Id = 2, Email = "test2@gmail.com", MobileNumber = "0633322444", Name = "Test user 2" }
            };



            var repository = A.Fake<ICustomerRepository>();

            var objectMapper = InquiryMapperBuilder.Build(new InquiryProfile());

            A.CallTo(() => repository.GetAllAsync()).Returns(mockEntities);

            var service = new CustomerService(repository, objectMapper);

            //Act
            var result = service.GetCustomers().Result;

            //Assert
            Assert.Equal(2, result.Length);
        }
    }
}
