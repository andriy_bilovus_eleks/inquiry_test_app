﻿using FakeItEasy;
using InquiryApp.AppServices.DomainModels;
using InquiryApp.AppServices.Mapper;
using InquiryApp.AppServices.Services;
using InquiryApp.DataServices.Entities;
using InquiryApp.DataServices.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Xunit;

namespace InquiryApp.Tests
{
    public class InquiryServiceTests
    {
        public InquiryServiceTests()
        {
            
        }

        [Fact]
        public void GetCustomer_FetchesCustomer()
        {
            //Arrange
            var mockEntity = new CustomerEntity { Id = 1, Email = "test" };

            var repository = A.Fake<ICustomerRepository>();

            var objectMapper = InquiryMapperBuilder.Build(new InquiryProfile());

            A.CallTo(() => repository.GetByCondition(A<Expression<Func<CustomerEntity, bool>>>._)).Returns(mockEntity);

            var service = new InquiryService(repository, objectMapper);

            //Act
            var result = service.GetCustomers(1, "test").Result;

            //Assert
            Assert.Equal(mockEntity.Id, result.Id);
            Assert.Equal(mockEntity.Email, result.Email);
        }
    }
}
