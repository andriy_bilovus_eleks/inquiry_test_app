﻿using InquiryApp.AppServices.DomainModels;
using InquiryApp.AppServices.Mapper.Contracts;
using InquiryApp.AppServices.Services.Contracts;
using InquiryApp.DataServices.Entities;
using InquiryApp.DataServices.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.AppServices.Services
{
    public class InquiryService : IInquiryService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IObjectMapper _mapper;

        public InquiryService(ICustomerRepository customerRepository, IObjectMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Customer> GetCustomers(int? id, string email)
        {
            var result = await _customerRepository.GetByCondition(c => c.Id == id || c.Email == email);
            return _mapper.Map<CustomerEntity, Customer>(result);
        }
    }
}
