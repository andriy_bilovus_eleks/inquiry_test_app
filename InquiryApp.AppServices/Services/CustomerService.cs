﻿using InquiryApp.AppServices.DomainModels;
using InquiryApp.AppServices.Mapper.Contracts;
using InquiryApp.AppServices.Services.Contracts;
using InquiryApp.DataServices.Entities;
using InquiryApp.DataServices.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.AppServices.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IObjectMapper _mapper;


        public CustomerService(ICustomerRepository customerRepository, IObjectMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Customer[]> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();
            return _mapper.Map<CustomerEntity[], Customer[]>(customers);
        }

        public async Task<Customer> AddCustomer(Customer customer)
        {
            var customerEntity = _mapper.Map<Customer, CustomerEntity>(customer);
            _customerRepository.Add(customerEntity);
            await _customerRepository.CommitAsync();
            return _mapper.Map<CustomerEntity, Customer>(customerEntity);
        }

        public async Task<Customer> UpdateCustomer(Customer customer)
        {
            var customerEntity = _mapper.Map<Customer, CustomerEntity>(customer);
            _customerRepository.MarkUpdate(customerEntity);
            await _customerRepository.CommitAsync();
            return _mapper.Map<CustomerEntity, Customer>(customerEntity);
        }
    }
}
