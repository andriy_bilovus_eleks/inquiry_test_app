﻿using InquiryApp.AppServices.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.AppServices.Services.Contracts
{
    public interface ICustomerService
    {
        Task<Customer[]> GetCustomers();

        Task<Customer> AddCustomer(Customer customer);

        Task<Customer> UpdateCustomer(Customer customer);
    }
}
