﻿using InquiryApp.AppServices.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InquiryApp.AppServices.Services.Contracts
{
    public interface IInquiryService
    {
        Task<Customer> GetCustomers(int? id, string email);
    }
}
