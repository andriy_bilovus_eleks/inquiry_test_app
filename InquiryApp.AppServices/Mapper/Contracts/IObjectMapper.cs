﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InquiryApp.AppServices.Mapper.Contracts
{
    public interface IObjectMapper
    {
        TResult Map<TInput, TResult>(TInput input);

        object Map(Type inputType, Type outputType, object input);
    }
}
