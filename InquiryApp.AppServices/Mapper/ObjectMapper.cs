﻿using System;
using AutoMapper;
using IObjectMapper = InquiryApp.AppServices.Mapper.Contracts.IObjectMapper;

namespace InquiryApp.AppServices.Mapper
{
    public class ObjectMapper : IObjectMapper
    {
        private readonly IMapper _mapper;

        internal ObjectMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public TResult Map<TInput, TResult>(TInput input) => _mapper.Map<TInput, TResult>(input);
        public object Map(Type inputType, Type outputType, object input) => _mapper.Map(input, inputType, outputType);
    }
}
