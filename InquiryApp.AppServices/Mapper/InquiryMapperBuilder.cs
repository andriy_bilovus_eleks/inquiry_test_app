﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace InquiryApp.AppServices.Mapper
{
    public static class InquiryMapperBuilder
    {
        public static ObjectMapper Build(Profile profile)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                cfg.AddProfile(profile);
            });

            return new ObjectMapper(config.CreateMapper());
        }
    }
}
