﻿using AutoMapper;
using InquiryApp.AppServices.DomainModels;
using InquiryApp.Common.Enums;
using InquiryApp.DataServices.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace InquiryApp.AppServices.Mapper
{
    public class InquiryProfile : Profile
    {
        public InquiryProfile()
        {
            CreateMap<TransactionEntity, Transaction>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.ToString()));

            CreateMap<Transaction, TransactionEntity>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Enum.Parse<TransactionStatus>(src.Status, true)));
        }
    }
}
