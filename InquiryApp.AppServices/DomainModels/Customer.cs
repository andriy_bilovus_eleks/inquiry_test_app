﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InquiryApp.AppServices.DomainModels
{
    public class Customer
    {
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; }

        [EmailAddress]
        [MaxLength(25)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        public Transaction[] Transactions { get; set; }
    }
}
